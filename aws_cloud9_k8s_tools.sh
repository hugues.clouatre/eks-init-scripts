#!/bin/bash
#
# Author: Hugues Clouâtre
# Version: 2021-01-13

printf "\n"
printf "This script upgrades aws-cli and aws-sam-cli to the latest version.\n"
printf "It also installs the latest versions of ec2-instance-selection, eksctl, kubectl and helm.\n"
printf "Plus a few utilities such as bash-completion, jq and moreutils.\n"
printf "It takes a few seconds to run.\n"
printf "\n"

# Set variables
NEWPATH=${HOME}/.local/bin:${HOME}/bin:${PATH}
BASH_PROFILE=${HOME}/.bash_profile
TEMP_DIR=/tmp
LOCAL_DIR=${HOME}/.local
LOCAL_BIN_DIR=${LOCAL_DIR}/bin

# Set versions
AWSCLI_VERSION=$(curl -sL https://github.com/aws/aws-cli/releases | grep 'href="/aws/aws-cli/releases/tag/2.[0-99]*.[0-99]*\"' | grep -v no-underline | head -n 1 | cut -d '"' -f 2 | awk '{n=split($NF,a,"/");print a[n]}' | awk 'a !~ $0{print}; {a=$0}')
AWSCLI_VERSION_LOCAL=$([ -f "${LOCAL_BIN_DIR}/aws" ] && "${LOCAL_BIN_DIR}/aws" --version | awk -F '[/ ]' '{print $2}' || aws --version | awk -F '[/ ]' '{print $2}')
AWSSAMCLI_VERSION=$(curl -sL https://github.com/aws/aws-sam-cli/releases | grep 'href="/aws/aws-sam-cli/releases/tag/v1.[0-99]*.[0-99]*\"' | grep -v no-underline | head -n 1 | cut -d '"' -f 2 | awk '{n=split($NF,a,"/");print a[n]}' | awk 'a !~ $0{print}; {a=$0}' | tr -d 'v')
AWSSAMCLI_VERSION_LOCAL=$([ -f "${LOCAL_BIN_DIR}/sam" ] && "${LOCAL_BIN_DIR}/sam" --version | awk -F '[ ]' '{print $4}' || sam --version | awk -F '[ ]' '{print $4}')

# Create BIN folder
[ ! -d "${LOCAL_BIN_DIR}" ] && mkdir "${LOCAL_BIN_DIR}"

# Change PATH order
if ! grep -q "${NEWPATH}" "${BASH_PROFILE}"; then
sed -i 's/^PATH=.*/PATH=$HOME\/.local\/bin:$HOME\/bin:$PATH/' "${BASH_PROFILE}"
fi

# Upgrade aws-sam-cli
if (( $(echo "${AWSSAMCLI_VERSION}" "${AWSSAMCLI_VERSION_LOCAL}" | awk '{print ($1 > $2)}') ));
then
  printf "Upgrading AWS Serverless Application Model (SAM) to version %s" "${AWSSAMCLI_VERSION}"
  pip3 install --user --upgrade --quiet --no-input aws-sam-cli
  printf " — done!\n"
else
  printf "The AWS Serverless Application Model (SAM) is already at latest version %s\n" "${AWSSAMCLI_VERSION}"
fi

# Upgrade aws-cli
if (( $(echo "${AWSCLI_VERSION}" "${AWSCLI_VERSION_LOCAL}" | awk '{print ($1 > $2)}') ));
then
  printf "Upgrading AWS Command Line Interface (CLI) to version %s" "${AWSCLI_VERSION}"
  curl -sL "https://awscli.amazonaws.com/awscli-exe-linux-x86_64-${AWSCLI_VERSION}.zip" -o "${TEMP_DIR}/awscli.zip" && unzip -qqo "${TEMP_DIR}/awscli.zip" -d "${TEMP_DIR}" && "${TEMP_DIR}/aws/install" -i "${LOCAL_DIR}/aws-cli" -b "${LOCAL_BIN_DIR}" --update > /dev/null 2>&1
  printf " — done!\n"
else
  printf "The AWS Command Line Interface (CLI) is already at latest version %s\n" "${AWSCLI_VERSION}"
fi

### k8s tools installation

# Set variables
NEWPATH=${HOME}/.local/bin:${HOME}/bin:${PATH}
BASH_PROFILE=${HOME}/.bash_profile
BASHRC=${HOME}/.bashrc
EKSCTL_LOCATION=${LOCAL_BIN_DIR}/eksctl
KUBECTL_LOCATION=${LOCAL_BIN_DIR}/kubectl
HELM_LOCATION=${LOCAL_BIN_DIR}/helm
BC_DEFAULT=/usr/share/bash-completion/bash_completion
BC_LOCAL=${HOME}/.bash_completion
KUBE_FOLDER=${HOME}/.kube

# Set versions
AWSEC2IS_VERSION=$(curl -sL https://github.com/aws/amazon-ec2-instance-selector/releases | grep 'href="/aws/amazon-ec2-instance-selector/releases/tag/v2.[0-99]*.[0-99]*\"' | grep -v no-underline | head -n 1 | cut -d '"' -f 2 | awk '{n=split($NF,a,"/");print a[n]}' | awk 'a !~ $0{print}; {a=$0}')
EKSCTL_VERSION=$(curl -sL https://github.com/weaveworks/eksctl/releases | grep 'href="/weaveworks/eksctl/releases/tag/0.[0-99]*.[0-9]*\"' | grep -v no-underline | head -n 1 | cut -d '"' -f 2 | awk '{n=split($NF,a,"/");print a[n]}' | awk 'a !~ $0{print}; {a=$0}')
KUBECTL_VERSION=$(curl -sL https://storage.googleapis.com/kubernetes-release/release/stable.txt)
HELM_VERSION=$(curl -sL https://github.com/helm/helm/releases | grep 'href="/helm/helm/releases/tag/v3.[0-9]*.[0-9]*\"' | grep -v no-underline | head -n 1 | cut -d '"' -f 2 | awk '{n=split($NF,a,"/");print a[n]}' | awk 'a !~ $0{print}; {a=$0}')

# Set user file-creation mode
umask 077

# Change PATH order
if ! grep -q "${NEWPATH}" "${BASH_PROFILE}"; then
sed -i 's/^PATH=.*/PATH=$HOME\/.local\/bin:$HOME\/bin:$PATH/' "${BASH_PROFILE}"
fi

# Install ec2-instance-selector
printf "\n"
printf "Installing or upgrading Amazon EC2 Instance Selector to version %s" "${AWSEC2IS_VERSION}"
curl -sL "https://github.com/aws/amazon-ec2-instance-selector/releases/download/${AWSEC2IS_VERSION}/ec2-instance-selector-linux-amd64.tar.gz" | tar xz -C "${LOCAL_BIN_DIR}"
printf " — done!\n"

# Install eksctl
printf "Installing or upgrading eksctl to version %s" "${EKSCTL_VERSION}"
curl -sL "https://github.com/weaveworks/eksctl/releases/download/${EKSCTL_VERSION}/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C "${LOCAL_BIN_DIR}"
printf " — done!\n"

# Install kubectl
printf "Installing or upgrading Kubernetes CLI to version %s" "${KUBECTL_VERSION}"
curl -sL "https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl" -o "${KUBECTL_LOCATION}" && chmod +x "${KUBECTL_LOCATION}"
printf " — done!\n"

# Install helm
printf "Installing or upgrading Helm to version %s" "${HELM_VERSION}"
curl -sL "https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz" | tar xz -C "${LOCAL_BIN_DIR}" --strip-components 1 linux-amd64/helm
printf " — done!\n"

# Install bash-completion, see https://github.com/scop/bash-completion
[ ! -f "${BC_DEFAULT}" ] && sudo yum -y -q install bash-completion

# Install jq, see https://stedolan.github.io/jq/
[ ! "command -v jq" ] && sudo yum -y -q install jq

# install gettext, see https://www.gnu.org/software/gettext/
[ ! "command -v envsubst" ] && sudo yum -y -q install gettext

# Configure bash-completion loading
if ! grep -q bash-completion "${BASHRC}"; then
cat >> "${BASHRC}" <<EOF

# Use bash-completion, if available
[ -f "${BC_DEFAULT}" ] && . "${BC_DEFAULT}"
EOF
fi

# Configure bash-completion for tools
true > "${BC_LOCAL}"
{
  "${EKSCTL_LOCATION}" completion bash
  "${HELM_LOCATION}" completion bash
  "${KUBECTL_LOCATION}" completion bash 
} >> "${BC_LOCAL}"
printf "\n"
printf "Please run the following command to enable bash-completion for the current session :\n"
printf "  source %s\n" "${BASHRC}"

# Add k alias with completion for kubectl
if ! grep -q kubectl "${BASHRC}"; then
cat >> "${BASHRC}" <<EOF

# Add k alias for kubectl
alias k=kubectl

# Add bash-completion for the k alias
complete -F __start_kubectl k
EOF
fi

# Configure kubectl
[ ! -d "${KUBE_FOLDER}" ] && mkdir "${KUBE_FOLDER}" && touch "${KUBE_FOLDER}/config"
printf "\n"
printf "Please run the following command to configure kubectl for existing clusters :\n"
printf "  aws eks update-kubeconfig --name <cluster_name> --region <region_code>\n"
printf "\n"
